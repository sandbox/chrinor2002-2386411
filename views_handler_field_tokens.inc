<?php

/**
 * @file
 * Definition of views_handler_field_tokens.
 */

/**
 */
class views_handler_field_tokens extends views_handler_field {

  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['token_text'] = array(
      'default' => '',
    );
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['warning'] = array(
      '#markup' => t('NOTE: this field requires a full entity to be loaded.  It will call entity_load for EACH row returned.  This can be a performance issue.'),
    );
    $form['token_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Replacement Text'),
      '#default_value' => $this->options['token_text'],
    );

    if (module_exists('token')) {
      $form['token_tree'] = array(
        '#theme' => 'token_tree',
        '#type' => 'fieldset',
        '#title' => t('Available Replacement Tokens'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#token_types' => array($this->definition['token_type']),
      );
    }
    else {
      $form['token_tree'] = array(
        '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>',
      );
    }
  }

  function render($values) {
    $entity_type = $this->definition['entity_type'];
    $token_type = $this->definition['token_type'];

    $source = $this->options['token_text'];
    $key = $this->view->base_field;

    // check for relationship
    if($this->relationship){
      // TODO: is there a better way to get this? (feels like there should be...)
      $info = entity_get_info($entity_type);
      $key = $this->relationship . '_'.$info['entity keys']['id'];
    }
    
    $target_id = $values->$key;
    if($target_id){
      $entity = entity_load($entity_type, array($target_id));

      $data = array();
      $data[$token_type] = $entity[$target_id];

      return token_replace($source, $data);
    }else{
      return NULL;
    }
  }

}

