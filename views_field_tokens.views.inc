<?php

/**
 * Implements hook_views_data_alter().
 */
function views_field_tokens_views_data_alter(&$data) {

  foreach (entity_get_info() as $type => $info) {

    $views_key = $info['base table'];

    if(!isset($data[$views_key])){
      continue;
    }

    $data[$type.'_tokens'] = array(
      'table' => array(
        'group' => $data[$views_key]['table']['group'],
        'join' => array(
          //'#global' => array()
          $views_key => array(),
        )
      ),
    );
    $data[$type.'_tokens']['tokens'] = array(
      'title' => t('@type Tokens', array('@type' => $info['label'])),
      'help' => t('Tokens related to the entity type of @type.', array('@type' => $info['label'])),
      'field' => array(
        'handler' => 'views_handler_field_tokens',
        'entity_type' => $type,
        'token_type' => $info['token type'],
      ),
    );
  }

}

